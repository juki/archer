;;;; Copyright (c) 2016 Juha Kiiski

;;;; archer.asd
;;;;   - Defines the main system for the ARCHER library.

(in-package #:asdf-user)

(defsystem "archer"
  :author "Juha Kiiski"
  :license "MIT License"
  :version "1.0.0"
  :description "An implementation of Clojure-like arrow macros."
  :long-description "See (DESCRIBE (FIND-PACKAGE :archer)) for details."
  
  :depends-on (:alexandria              ;Public domain
               )
  
  :serial t
  :components ((:static-file "readme.org")
               (:static-file "license")
               (:file "package")
               (:file "arrows")
               (:file "arrow-defuns"))

  :in-order-to ((test-op (test-op :archer-test))))
