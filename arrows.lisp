;;;; Copyright (c) 2016 Juha Kiiski

;;;; arrows.lisp
;;;;   - Implements basic arrow macros.

(in-package #:archer)

;; Some helpers used during macroexpansion.
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun thread-forms (insert-function initial forms)
    "Use INSERT-FUNCTION to insert the INITIAL value into the first of
FORMS, then insert the result to the next form, and so on.

Used to implement the arrow macros."
    (if (null forms)
        initial
        (loop for form in (mapcar #'ensure-list forms)
              for result = (funcall insert-function initial form)
                then (funcall insert-function result form)
              finally (return result))))
  
  (defun compare-symbols (a b)
    "Compare string designators with STRING=.

Will return NIL, rather than signaling an error, if either A or B is
not a string designator.

Intended to be used as a :TEST argument when searching for a symbol in
a list that contains elements other than symbols as well."
    (and (typep a 'string-designator)
         (typep b 'string-designator)
         (string= a b)))

  ;; The next four functions are meant to be used as the
  ;; INSERT-FUNCTION for the different arrow macros (-> ->> -<> -<>>).
  
  (defun append-arg (item list)
    "Create a copy of LIST with ITEM inserted as the first argument (second element).

Used for -> arrows."
    (list* (first list) item (rest list)))

  (defun prepend-arg (item list)
    "Create a copy of LIST with ITEM inserted as the last argument (last element).

Used for ->> arrows."
    (append list (list item)))
  
  (defun substitute-<>-or-append (item list)
    "Create a copy of LIST with a single <> placeholder replaced with
ITEM. If there is no <>, add item as the first argument (second
element).

Used for -<> arrows."
    (if (member "<>" list :test #'compare-symbols)
        (substitute item "<>" list :test #'compare-symbols :count 1)
        (append-arg item list)))
  
  (defun substitute-<>-or-prepend (item list)
    "Create a copy of LIST with a single <> placeholder replaced with
ITEM. If there is no <>, add item as the last argument (last
element).

Used for -<>> arrows."
    (if (member "<>" list :test #'compare-symbols)
        (substitute item "<>" list :test #'compare-symbols :count 1)
        (prepend-arg item list))))


;;; Simple arrows

(defmacro define-arrow-macro (arrow insert-function docstring)
  "Define an arrow macro.

INSERT-FUNCTION should be a function of two arguments, an ITEM and a
FORM. The ITEM is the initial value or the last result that should be
inserted into FORM as is appropriate for the arrow in question."
  `(defmacro ,arrow (initial &body forms)
     ,@(unless (null docstring) `(,docstring))
     (thread-forms (ensure-function ',insert-function) initial forms)))

(define-arrow-macro -> append-arg
  "Evaluate the first of FORMS with INITIAL as its first argument,
then evaluate the rest of FORMS using the previous result as the first
argument.

For example,
    (-> x (+ 10) (/ 100))
is equivalent of
    (/ (+ x 10) 100)

FORMS can be atoms too, which will wrapped in a list. So
    (-> x 1+ (* 2))
is equivalent of
    (* (1+ x) 2)")

(define-arrow-macro ->> prepend-arg
  "Like ->, but the result is the last argument.

For example,
    (->> x (+ 10) (/ 100))
is equivalent of
    (/ 100 (+ 10 x))")

(define-arrow-macro -<> substitute-<>-or-append
  "Like ->, but if there is a <> placeholder on the top level of a
form, the argument is inserted there.

For example,
    (-<> x (+ 10) (/ 100 <>))
is equivalent of
    (/ 100 (+ x 10))")

(define-arrow-macro -<>> substitute-<>-or-prepend
  "Like ->>, but if there is a <> placeholder on the top level of a
form, the argument is inserted there.

For example,
    (-<>> x (+ <> 10) (/ 100))
is equivalent of
    (/ 100 (+ x 10))")


;;; AS macro

(defmacro as-> ((var initial) &body forms)
  "Arrow macro where the initial/result is bound to VAR.

For example,
    (as-> (x 10) (* x x) (+ 10 x))
is equivalent of
    (let ((x 10))
      (setf x (* x x))
      (+ 10 x))

There may be declarations after VAR."
  (multiple-value-bind (forms dec doc) (parse-body forms)
    (declare (ignore doc))
    `(let ((,var ,initial))
       ,@(unless (null dec) dec)
       ,@(mapcar (lambda (form)
                   `(setf ,var ,form))
                 (butlast forms))
       ,(or (lastcar forms)
            var))))


;;; SOME arrows

(defmacro define-some-arrow-macro (arrow insert-function docstring)
  `(defmacro ,arrow (initial &body forms)
     ,docstring
     (with-gensyms (some-block)
       `(block ,some-block
          ,(thread-forms (lambda (item form)
                           `(or ,(funcall (ensure-function ',insert-function) item form)
                                (return-from ,some-block (values nil t))))
                         initial forms)))))

(define-some-arrow-macro some-> append-arg
  "Like ->, but returns immediately if any of FORMS returns NIL.

If the evaluation short-circuits, returns (VALUES NIL T).

For example,
    (some-> (list :bar (list :foo 2))
      (getf :bar)
      (getf :foo)
      (* 2))
will return 4. If the number was not found, it returns NIL instead of
causing an error.")

(define-some-arrow-macro some->> prepend-arg
  "Like ->>, but returns immediately if any of FORMS returns NIL.

If the evaluation short-circuits, returns (VALUES NIL T).")

(define-some-arrow-macro some-<> substitute-<>-or-append
  "Like -<>, but returns immediately f any of FORMS returns NIL.

If the evaluation short-circuits, returns (VALUES NIL T).")

(define-some-arrow-macro some-<>> substitute-<>-or-prepend
  "Like -<>>, but returns immediately f any of FORMS returns NIL.

If the evaluation short-circuits, returns (VALUES NIL T).")


;;; COND arrows

(defmacro define-cond-arrow-macro (arrow insert-function docstring)
  `(defmacro ,arrow (initial &body forms)
     ,docstring
     (with-gensyms (var)
       (let ((insert (ensure-function ',insert-function)))
         `(let ((,var ,initial))
            ,@(loop for (test form) in forms
                    collect `(when ,test
                               (setf ,var ,(funcall insert var (ensure-list form)))))
            ,var)))))

(define-cond-arrow-macro cond-> append-arg
  "Like ->, but FORMS consists of (TEST FORM) pairs. The FORM is only
evaluated if the TEST returns true. Notice that the initial/result
will not be inserted into the test.

For (a silly) example,

    (defun test (n)
      (cond-> n
        ((minusp n) -) ;make positive
        (t sqrt)))

    (test 10)  ;=> 3.1622777
    (test -10) ;=> 3.1622777

The syntax differs from Clojure slightly; the pairs are enclosed in
parentheses.")

(define-cond-arrow-macro cond->> prepend-arg
  "Like COND->, but insert initial/result as the last argument.")

(define-cond-arrow-macro cond-<> substitute-<>-or-append
  "Like COND->, but allows for <> placeholder like -<>.")

(define-cond-arrow-macro cond-<>> substitute-<>-or-prepend
  "Like COND->>, but allows for <> placeholder like -<>>.")
