;;;; Copyright (c) 2016 Juha Kiiski

;;;; arrow-defuns.lisp
;;;;   - Implements DEFUN and LAMBDA variants of arrow macros.

(in-package #:archer)

(defmacro define-arrow-defun-macro (arrow &optional separatorp)
  `(defmacro ,(if separatorp
                  (symbolicate 'defun '- arrow)
                  (symbolicate 'defun arrow))
       (name (arg) &body forms)
     ,(format nil "Define a function with a single argument ~
                   that will be threaded through FORMS with ~a."
              arrow)
     (multiple-value-bind (forms declaration documentation)
         (parse-body forms :documentation t)
       `(defun ,name (,arg)
          ,@(unless (null documentation) `(,documentation))
          ,@(unless (null declaration) declaration)
          (,',arrow ,arg ,@forms)))))

(define-arrow-defun-macro ->)
(define-arrow-defun-macro ->>)
(define-arrow-defun-macro -<>)
(define-arrow-defun-macro -<>>)
(define-arrow-defun-macro some-> t)
(define-arrow-defun-macro some->> t)
(define-arrow-defun-macro some-<> t)
(define-arrow-defun-macro some-<>> t)
(define-arrow-defun-macro cond-> t)
(define-arrow-defun-macro cond->> t)
(define-arrow-defun-macro cond-<> t)
(define-arrow-defun-macro cond-<>> t)

(defmacro defun-as-> (name (var) &body forms)
  "Define a function with a single argument that will be threaded
through FORMS with AS->."
  (multiple-value-bind (forms declaration documentation)
      (parse-body forms :documentation t)
    `(defun ,name (,var)
       ,@(unless (null documentation) `(,documentation))
       (as-> (,var ,var)
         ,@(unless (null declaration) declaration)
         ,@forms))))

(defmacro define-arrow-lambda-macro (arrow &optional separatorp)
  (with-gensyms (arg)
    `(defmacro ,(if separatorp
                    (symbolicate 'lambda '- arrow)
                    (symbolicate 'lambda arrow)) (&body forms)
       ,(format nil "Make a lambda function with a single argument ~
                     that will be threaded through FORMS with ~a."
              arrow)
      `(lambda (,',arg) (,',arrow ,',arg ,@forms)))))

(define-arrow-lambda-macro ->)
(define-arrow-lambda-macro ->>)
(define-arrow-lambda-macro -<>)
(define-arrow-lambda-macro -<>>)
(define-arrow-lambda-macro some-> t)
(define-arrow-lambda-macro some->> t)
(define-arrow-lambda-macro some-<> t)
(define-arrow-lambda-macro some-<>> t)
(define-arrow-lambda-macro cond-> t)
(define-arrow-lambda-macro cond->> t)
(define-arrow-lambda-macro cond-<> t)
(define-arrow-lambda-macro cond-<>> t)

(defmacro lambda-as-> ((var) &body forms)
  "Make a lambda function with a single argument that will be threaded
through FORMS with AS->."
  (with-gensyms (arg)
    `(lambda (,arg)
       (as-> (,var ,arg) ,@forms))))
