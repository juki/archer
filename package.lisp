;;;; Copyright (c) 2016 Juha Kiiski

;;;; package.lisp
;;;;   - Defines the main package for the ARCHER library.
;;;;   - Exports all symbols intended for use outside the ARCHER library.
;;;;   - Documents the ARCHER library.

(in-package #:cl-user)

(defpackage #:archer
  (:use #:cl #:alexandria)
  (:export #:->
           #:->>
           #:-<>
           #:-<>>
           #:as->
           #:some->
           #:some->>
           #:some-<>
           #:some-<>>
           #:cond->
           #:cond->>
           #:cond-<>
           #:cond-<>>
           #:defun->
           #:defun->>
           #:defun-<>
           #:defun-<>>
           #:defun-some->
           #:defun-some->>
           #:defun-some-<>
           #:defun-some-<>>
           #:defun-as->
           #:defun-cond->
           #:defun-cond->>
           #:defun-cond-<>
           #:defun-cond-<>>
           #:lambda->
           #:lambda->>
           #:lambda-<>
           #:lambda-<>>
           #:lambda-some->
           #:lambda-some->>
           #:lambda-some-<>
           #:lambda-some-<>>
           #:lambda-as->
           #:lambda-cond->
           #:lambda-cond->>
           #:lambda-cond-<>
           #:lambda-cond-<>>)
  (:documentation "Defines Clojure-like arrow macros.

Arrow macros allow you to split up nested expressions into a
pipeline. For example,

    (setf (getf (gethash :foobar table) :quux) \"something\")

could be written as

    (-> (gethash :foobar table)
      (getf :quux)
      (setf \"something\"))

The first form is the initial value; it will be inserted into the next
form as the first argument. The resulting form will then be inserted
into the next, and so on.

The forms may also be just the name of a function. This will be
treated as a function call with a single argument.

    (-> 10 1+ sqrt) ;=> 3.3166249

The ->> arrow is like ->, but inserts the previous form as the last
argument. There are also -<> and -<>> (aka. diamond wands), which are
like -> and ->> respectively, but also allow you to put a <> symbol as
placeholder where you want the form to be inserted.

    (-<> table
      (gethash :foobar <>) ; Here the form needs to be the last argument
      (getf :quux)
      (setf \"something\"))

There are also SOME->, SOME->>, SOME-<>, SOME-<>>, which will return
immediately if any of the forms returns NIL. Do notice that the above
example could not be written using these, since the SETF would not
have a place as the first argument.

AS-> will assign the initial value to a variable, and then evaluate
each form in order, assigning the result to the variable. You can also
place declarations after the variable name. These will be placed on
top of the LET-block in the expansion.

The syntax for AS-> is slightly different from Clojure. In Clojure the
initial value is the first argument and the variable name is the
second. Here the binding is done similiarly to LET:

    (as-> (x 10)   ; In Clojure this would be (AS-> 10 x ...
      (1+ x) 
      (* x x))
    ;=> 121

COND-> and COND->> are like -> and ->>, but they take pairs of (TEST
FORM) instead of just forms. The FORM is only evaluated if the TEST is
true. The initial/result will not be inserted into the TEST
expressions. All clauses whose test is true are evaluated. The syntax
differs from Clojure a little, as the pairs are enclosed in parentheses.

    (let ((x 10))
      (cond-> x
        (t print)
        ((evenp x) (/ 2))
        ((oddp x) (* 2))))
    ;=> 5

For all of these there is a DEFUN and a LAMBDA version. These are
simple syntactic sugar for functions whose body can be expressed with
an arrow. The DEFUN versions can have a docstring and declarations.

    (mapcar (lambda-> 1+ (* 2))
            (list 1 2 3 4 5))
    ;=> (4 6 8 10 12)

The LAMBDA versions don't have a lambda-list. LAMBDA-AS-> has the
variable name given to AS-> in place of the lambda-list.

    (mapcar (lambda-as-> (x) (* x x) (/ x 2))
            (list 1 2 3 4 5))
    ;=> (1/2 2 9/2 8 25/2)

DEFUN-AS-> will use the same name both for the function argument and
the variable in AS->. Any declarations will be placed in the AS->
form, rather than in the DEFUN. For example,

    (defun-as-> foobar (x)
      \"Docstring\"
      (declare (type fixnum x))
      (* x x)
      (/ x 10))

will be expanded to

    (defun foobar (x)
      \"Docstring\"
      (as-> (x x)
        (declare (type fixnum x))
        (* x x)
        (/ x 10)))

so the declarations will be for the inner LET-block created by AS->,
instead of the DEFUN."))

