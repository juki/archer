;;;; Copyright (c) 2016 Juha Kiiski

;;;; archer-test.asd
;;;;   - Defines a test system for ARCHER.

(in-package #:asdf-user)

(defsystem "archer-test"
  :author "Juha Kiiski"
  :license "MIT License"
  :description "Tests for archer."

  :depends-on (:archer
               :lisp-unit2              ;MIT
               :uiop                    ;MIT
               ) 

  :components ((:module "test"
                :serial t
                :components ((:file "package")
                             (:file "arrows")
                             (:file "defuns"))))

  :perform (test-op (o s)
                    (let (*debugger-hook*)
                      (uiop:symbol-call :lisp-unit2 :print-summary
                                        (uiop:symbol-call :lisp-unit2 :run-tests
                                                          :package :archer-test)))))
