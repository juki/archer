;;;; Copyright (c) 2016 Juha Kiiski

;;;; arrows.lisp
;;;;   - Defines tests for the basic arrow macros.

(in-package #:archer-test)

(define-test test-arrows ()
  (assert-number-equal 110 (-> 10 1+ (* 100) (/ 10)))
  (assert-number-equal 10 (->> 10 (/ 100)))
  (assert-number-equal 1 (-<> 10 (1+ <>) (* <> 100) (/ 1100 <>)))
  (assert-number-equal 10 (-<> 10 1+ 1-))
  ;; All arrows should return the initial if there are no forms.
  (assert-number-equal 10 (-> 10))
  (assert-number-equal 10 (->> 10))
  (assert-number-equal 10 (-<> 10))
  (assert-number-equal 10 (-<>> 10)))

(define-test test-as-> ()
  (assert-number-equal 110 (as-> (x 10) (* x x) (+ x 10)))
  (assert-number-equal 10 (as-> (x 10))))

(define-test test-some-arrows ()
  (assert-equal 4 (some-> (list :bar (list :foo 2))
                    (getf :bar)
                    (getf :foo)
                    (* 2)))
  (assert-equal (values nil t) (some-> (list :bar nil)
                                 (getf :bar)
                                 (getf :foo)
                                 (* 2)))
  (assert-equal (list :bar "bar") (some-> (list :bar "bar")))
  (let ((ht (make-hash-table)))
    (setf (gethash :foo ht) 100)
    (assert-equal 200 (some->> ht
                        (gethash :foo)
                        (* 2))))
  (assert-equal (list :bar "bar") (some->> (list :bar "bar"))))

(define-test test-cond-arrows ()
  (assert-equal (cons (cons nil :foo) :bar)
                (cond-> nil
                  ((zerop 0) (cons :foo))
                  ((plusp 10) (cons :bar))))
  (assert-equal (list "positive" "odd")
                (let ((n 9))
                  (cond->> (list)
                    ((evenp n) (cons "even"))
                    ((oddp n) (cons "odd"))
                    ((plusp n) (cons "positive"))
                    ((zerop n) (cons "zero"))
                    ((minusp n) (cons "negative")))))
  (assert-equal 10 (cond-> 10)))
