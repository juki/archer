;;;; Copyright (c) 2016 Juha Kiiski

;;;; package.lisp
;;;;   - Defines a test package for ARCHER.

(in-package #:cl-user)

(defpackage #:archer-test
  (:use #:cl #:archer :lisp-unit2))
