;;;; Copyright (c) 2016 Juha Kiiski

;;;; defuns.lisp
;;;;   - Defines tests for the DEFUN and LAMBDA variants of the arrow
;;;;     macros.

(in-package #:archer-test)

(defun-> test-defun-> (x)
  "This is just a test function."
  1+
  (* 2)
  (/ 5))

(defun->> test-defun->> (x)
  1+
  (* 2)
  (/ 100))

(defun-<> test-defun-<> (x)
  1+
  (expt <> 2)
  (/ 100 <>))

(defun-<>> test-defun-<>> (x)
  1+
  (expt <> 2)
  (/ 100))

(define-test test-defuns ()
  (assert-number-equal 4 (test-defun-> 9))
  (assert-equal "This is just a test function."
                (documentation 'test-defun-> 'function))
  (assert-number-equal 10 (test-defun->> 4))
  (assert-number-equal 1 (test-defun-<> 9))
  (assert-number-equal 1 (test-defun-<>> 9)))

(define-test test-lambdas ()
  (assert-equal (list 1 2 10)
                (mapcar (lambda-> 1+ (* 2) (/ 10))
                        (list 4 9 49)))
  (assert-equal (list 10 5 1)
                (mapcar (lambda->> 1+ (* 2) (/ 100))
                        (list 4 9 49)))
  (assert-equal (list 1 2 10)
                (mapcar (lambda-<> 1+ (* 2 <>) (/ <> 10))
                        (list 4 9 49)))
  (assert-equal (list 10 5 1)
                (mapcar (lambda-<>> 1+ (* <> 2) (/ 100))
                        (list 4 9 49))))

(defun-cond-> test-cond-> (n)
  ((oddp n) (cons "odd"))
  ((evenp n) (cons "even")))

(defun-cond->> test-cond->> (n)
  ((oddp n) (cons "odd"))
  ((evenp n) (cons "even")))

(define-test test-cond-defuns ()
  (assert-equal (cons 3 "odd") (test-cond-> 3))
  (assert-equal (cons 4 "even") (test-cond-> 4))
  (assert-equal (cons "odd" 3) (test-cond->> 3))
  (assert-equal (cons "even" 4) (test-cond->> 4)))

(define-test test-lambda-conds ()
  (let ((x 10))
    (assert-equal 5 (funcall (lambda-cond->
                               ((minusp x) (* 2))
                               ((plusp x) (/ 2)))
                             10))
    (assert-float-equal 0.2 (funcall (lambda-cond->>
                                       ((minusp x) (* 2))
                                       ((plusp x) (/ 2)))
                                     10))))

(define-test test-lambda-as-arrow ()
  (assert-equal 26 (funcall (lambda-as-> (x)
                              (* x x)
                              (1+ x))
                            5)))
