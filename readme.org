#+TITLE: Archer

An implementation of Clojure-like arrow macros as well as =DEFUN= and
=LAMBDA= variants for them.

* =->=, =->>=, =-<>=, =-<>>=

Arrow macros allow you to split up nested expressions into a
pipeline. For example,

#+BEGIN_SRC lisp
  (setf (getf (gethash :foobar table) :quux) "something")
#+END_SRC

could be written as

#+BEGIN_SRC lisp
  (-> (gethash :foobar table)
    (getf :quux)
    (setf "something"))
#+END_SRC

The first form is the initial value; it will be inserted into the
next form as the first argument. The resulting form will then be
inserted into the next, and so on.

The forms may also be just the name of a function. This will be
treated as a function call with a single argument.

#+BEGIN_SRC lisp
  (-> 10 1+ sqrt) ;=> 3.3166249
#+END_SRC

The =->>= arrow is like =->=, but inserts the previous form as the
last argument. There are also =-<>= and =-<>>=, which are like =->=
and =->>= respectively, but also allow you to put a =<>= symbol as
placeholder where you want the form to be inserted.

#+BEGIN_SRC lisp
  (-<> table
    (gethash :foobar <>) ; Here the form needs to be the last argument
    (getf :quux)
    (setf "something"))
#+END_SRC

* =SOME->=, =SOME->>=, =SOME-<>=, =SOME-<>>=

These arrows will return immediately if any of the forms returns
NIL=. Do notice that the above example could not be written using
these, since the =SETF= would not have a place as the first
argument.

For example,

#+BEGIN_SRC lisp
  (some-> (list :bar (list :foo 2))
    (getf :bar)
    (getf :foo)
    (* 2))
  ;=> 4
#+END_SRC

If =:BAR= or =:FOO= was not found, =NIL= would be returned rather
than signalling an error.

* =COND->=, =COND->>=, =COND-<>=, =COND-<>>=

These arrows take pairs of =(TEST FORM)= instead of just forms. The
FORM= is only evaluated if the =TEST= is true. The initial/result
will not be inserted into the =TEST= expressions. All clauses whose
test is true are evaluated. The syntax differs from Clojure a
little, as the pairs are enclosed in parentheses.

#+BEGIN_SRC lisp
  (let ((x 10))
    (cond-> x
      (t print)
      ((evenp x) (/ 2))
      ((oddp x) (* 2))))
  ;=> 5
#+END_SRC

* =AS->=

AS->= will assign the initial value to a variable, and then
evaluate each form in order, assigning the result to the
variable. You can also place declarations after the variable
name. These will be placed on top of the =LET=-block in the
expansion.

The syntax for =AS->= is slightly different from Clojure. In Clojure
the initial value is the first argument and the variable name is the
second. Here the binding is done similiarly to =LET=:

#+BEGIN_SRC lisp
  (as-> (x 10)   ; In Clojure this would be (AS-> 10 x ...
    (1+ x) 
    (* x x))
  ;=> 121
#+END_SRC

* =DEFUN= and =LAMBDA= versions

For all of these there is a =DEFUN= and a =LAMBDA= version. These
are simple syntactic sugar for functions whose body can be expressed
with an arrow. The =DEFUN= versions can have a docstring and
declarations.

#+BEGIN_SRC lisp
  (defun->> foobar (x)
    1+
    (/ 1000))
  (foobar 9) ;=> 100

  (mapcar (lambda-> 1+ (* 2))
          (list 1 2 3 4 5))
  ;=> (4 6 8 10 12)
#+END_SRC

The =LAMBDA= versions don't have a lambda-list. =LAMBDA-AS->= has
the variable name given to =AS->= in place of the lambda-list.

#+BEGIN_SRC lisp
  (mapcar (lambda-as-> (x) (* x x) (/ x 2))
          (list 1 2 3 4 5))
  ;=> (1/2 2 9/2 8 25/2)
#+END_SRC
  
DEFUN-AS->= will use the same name both for the function argument
and the variable in =AS->=. Any declarations will be placed in the
AS->= form, rather than in the =DEFUN=. For example,

#+BEGIN_SRC lisp
  (defun-as-> foobar (x)
    "Docstring"
    (declare (type fixnum x))
    (* x x)
    (/ x 10))
#+END_SRC

will be expanded to

#+BEGIN_SRC lisp
  (defun foobar (x)
    "Docstring"
    (as-> (x x)
      (declare (type fixnum x))
      (* x x)
      (/ x 10)))
#+END_SRC

* License
  
MIT License
  
* Author

Juha Kiiski <juki_pub@outlook.com>
